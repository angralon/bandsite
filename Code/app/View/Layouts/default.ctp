<?php
/**
 * My Theme for Croogo
 *
 * @author Michael Schär
 */
?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
	<script type="text/javascript">
	/* <![CDATA[ */
		(function() {
		    var s = document.createElement('script'), t = document.getElementsByTagName('script')[0];
		    s.type = 'text/javascript';
		    s.async = true;
		    s.src = 'http://api.flattr.com/js/0.6/load.js?mode=auto';
		    t.parentNode.insertBefore(s, t);
		})();
	/* ]]> */
	</script>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<title><?php echo $title_for_layout; ?> &raquo; <?php echo Configure::read('Site.title'); ?></title>
	<?php
		echo $this->Layout->meta();
		echo $this->Layout->feed();
		echo $this->Html->css(array(
			'reset',
			'960',
			'theme',
		));
		echo $this->Layout->js();
		echo $this->Html->script(array(
			'jquery/jquery.min',
			'jquery/jquery.hoverIntent.minified',
			'jquery/superfish',
			'jquery/supersubs',
			'theme',
		));
		echo $this->Blocks->get('css');
		echo $this->Blocks->get('script');
	?>
</head>
<body>
	<div id="wrapper">
		<div id="nav">
			<?php echo $this->Layout->menu('main', array('dropdown' => true)); ?>
		</div>
		<div id="bgWrapper">

			<div id="headSpacer" style="background: #fff url('<?php echo $this->Layout->headerImage() ?>') no-repeat;"></div>
			<div class="clear"></div>


			<div id="main" class="container_16">
				<div id="content" class="grid_11">
				<?php
					echo "<br />";
					echo $this->Layout->sessionFlash();
					echo $content_for_layout;
				?>
				</div>
	
				<div id="sidebar" class="grid_5">
				<?php echo $this->Layout->blocks('right'); ?>
				</div>
	
				<div class="clear"></div>
			</div>
	
			<div id="footer">
				<div class="container_16">
					<div class="grid_8 left">&copy;
						<?php 
							$copyYear = 2012; 
							$curYear = date('Y'); 
							echo $copyYear . (($copyYear != $curYear) ? '-' . $curYear : '');
						?> Schär Michael
					</div>
					<div class="grid_8 right">
						<!-- Flattr Button -->
						<a class="FlattrButton" style="display:none;" rev="flattr;button:compact;" href="http://mythares.de/Band/"></a>
						<noscript><a href="http://flattr.com/thing/1074637/BandName" target="_blank">
						<img src="http://api.flattr.com/button/flattr-badge-large.png" alt="Flattr this" title="Flattr this" border="0" /></a></noscript>
					</div>
					<div class="clear"></div>
				</div>
			</div>
		</div>
	</div>
	<?php
		echo $this->Blocks->get('scriptBottom');
		echo $this->Js->writeBuffer();
	?>
	</body>
</html>
